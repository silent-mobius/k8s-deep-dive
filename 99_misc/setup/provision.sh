#!/usr/bin/env bash 

modprobe br_netfilter
sudo echo 'net.bridge.bridge-nf-call-iptables=1' >> /etc/sysctl.conf
sudo echo 'net.ipv4.ip_forward=1' >> /etc/sysctl.conf
sudo sysctl --system

swapoff -a

#sed -i 's///' /etc/fstab

curl -L get.docker.com|bash

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list 

sudo apt update && apt install -y kubeadm kubelet kubectl

sed -i s'/disabled_plugins/#disabled_plugins/' /etc/containerd/config.toml

sudo systemctl restart containerd

echo "source <(kubectl completion bash)" >> ~/.bashrc
echo 'alias k=kubectl' >>~/.bashrc

if [ $(hostname) == 'k8s-master' ];then
    IP_ADDR=$(ip -br addr sh|grep UP|awk '{print $3}'|tail -n 1)

    kubeadm init --apiserver-advertise-address=${IP_ADDR%/*} --pod-network-cidr=10.244.0.0/16 
    
    mkdir -p $HOME/.kube

    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

    kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
    #kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')" 2> /dev/null
    #kubectl apply https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml 
fi



#  I look for it in the flannel's pod configuration. If you download the kube-flannel.yml file
# you should look at **DaemonSet** spec, specifically at the "kube-flannel" container. 
# There, you should add the required "--iface=enp0s8" argument (Don't forget the "="). Part of the code I've used.

#   containers:
#   - name: kube-flannel
#     image: quay.io/coreos/flannel:v0.10.0-amd64
#     command:
#     - /opt/bin/flanneld
#     args:
#     - --ip-masq
#     - --kube-subnet-mgr
#     - --iface=enp0s8

# Then run kubectl apply -f kube-flannel.yml

# Hope helps.